import os

from jinja2 import Environment
import pytest


@pytest.fixture
def jinja2_env():
    return Environment(
        extensions=["jinja2_ansible_filters.AnsibleCoreFiltersExtension"])


FILTERS = {"b64decode": {"params": "MTIzYWJj",
                         "validate": "123abc"},
           "b64encode": {"params": "123abc",
                         "validate": "MTIzYWJj"},
           "basename": {"params": "123/test/abc.py",
                        "validate": "abc.py"},
           "bool": ({"params": 1,
                     "validate": True},
                    {"params": 0,
                     "validate": False},
                    {"params": "True",
                     "validate": True},
                    {"params": "False",
                     "validate": False},
                    {"params": None,
                     "validate": None}),
           "checksum": {"params": "abc123",
                        "validate": "6367c48dd193d56ea7b0baad25b19455e529f5ee"},
           "combine": (
               {"params": (
                   {"a": {"x": "default", "y": "default"},
                    "b": "default", "c": "default"},
                   {"a": {"y": "patch", "z": "patch"}, "b": "patch"}
               ),
                   "kwargs": {"recursive": False},
                   "validate": {"a": {"z": "patch", "y": "patch"},
                                "b": "patch", "c": "default"}
               },
               {
                   "params": ({"a": {"x": "default", "y": "default"},
                               "b": "default",
                               "c": "default"},
                              {"a": {"y": "patch",
                                     "z": "patch"}, "b": "patch"}),
                   "kwargs": {"recursive": True},
                   "validate": {"a": {"x": "default",
                                "z": "patch", "y": "patch"},
                                "b": "patch", "c": "default"}
               },
               {
                   "params": ({"a": ["default"]}, {"a": ["patch"]}),
                   "kwargs": {"list_merge": 'replace'},
                   "validate": {"a": ["patch"]}
               },
               {
                   "params": ({"a": ["default"]}, {"a": ["patch"]}),
                   "kwargs": {"list_merge": 'keep'},
                   "validate": {"a": ["default"]}
               },
               {
                   "params": ({"a": ["default"]}, {"a": ["patch"]}),
                   "kwargs": {"list_merge": "append"},
                   "validate": {"a": ["default", "patch"]}
               },
               {
                   "params": ({"a": ["default"]}, {"a": ["patch"]}),
                   "kwargs": {"list_merge": 'prepend'},
                   "validate": {"a": ["patch", "default"]}
               },
               {
                   "params": ({"a": [1, 1, 2, 3]}, {"a": [3, 4, 5, 5]}),
                   "kwargs": {"list_merge": 'append_rp'},
                   "validate": {"a": [1, 1, 2, 3, 4, 5, 5]}
               },
               {
                   "params": ({"a": [1, 1, 2, 3]}, {"a": [3, 4, 5, 5]}),
                   "kwargs": {"list_merge": 'prepend_rp'},
                   "validate": {"a": [3, 4, 5, 5, 1, 1, 2]}
               },
               {
                   "params": (
                       {"a": {"a'": {"b": "value", "y": "value",
                                     "list": ["value"]}, "b": [1, 1, 2, 3]}},
                       {"a": {"a'": {"z": "patch_value", "y": "patch_value",
                              "list": ["patch_value"]},
                              "b": [3, 4, 4, {"key": "value"}]}}),
                   "kwargs": {"recursive": True, "list_merge": 'append_rp'},
                   "validate": {'a': {"a'": {'b': 'value', 'y': 'patch_value',
                                      'list': ['value', 'patch_value'],
                                             'z': 'patch_value'},
                                      'b': [1, 1, 2, 3, 4, 4,
                                            {'key': 'value'}]}}
               }),
           "comment": {"params": "abc123",
                       "validate": "#\n# abc123\n#"},
           "dirname": {"params": "123/test/abc.py",
                       "validate": "123/test"},
           "expanduser": {"params": "~root/abc.py",
                          "validate": os.path.expanduser("~root/abc.py")},
           "expandvars": {"params": "$HOME/abc.py",
                          "validate": os.path.expandvars("$HOME/abc.py")},
           "extract": {"params": ("test", {"test": "things"}),
                       "validate": "things"},
           "fileglob": {"params": "tests/*.py",
                        "validate": lambda x: "tests/test_extension.py" in x},
           "flatten": {"params": [1, 2, 3, ['a', 'b', 'c']],
                       "validate": [1, 2, 3, 'a', 'b', 'c']},
           "from_json": {"params": '{"1": "a", "2": "b"}',
                         "validate": {"1": "a", "2": "b"}},
           "from_yaml": ({"params": '{"1": "a", "2": "b"}',
                          "validate": {"1": "a", "2": "b"}},
                         {"params": 123,
                          "validate": 123}),
           "from_yaml_all": {"params": '{"1": "a", "2": "b"}',
                             "validate":
                                 lambda x: next(x) == {'1': 'a', '2': 'b'}},
           "ans_groupby": {"validate": lambda x: hasattr(x, '__call__')},
           "hash": {"params": "abc",
                    "validate": "a9993e364706816aba3e25717850c26c9cd0d89d"},
           "mandatory": {"params": "abc123",
                         "validate": "abc123"},
           "md5": {"params": "abc",
                   "validate": "900150983cd24fb0d6963f7d28e17f72"},
           "quote": {"params": "abc123",
                     "validate": "abc123"},
           "ans_random": {"validate": lambda x: hasattr(x, '__call__')},
           "random_mac": ({"params": "123abc",
                           "validate": lambda x: "123abc" in x},
                          {"params": ("123abc", 1234567),
                           "validate": lambda x: "123abc" in x}),
           "realpath": {"params": "/",
                        "validate": "/"},
           "regex_escape": {"params": "abc123*",
                            "validate": "abc123\\*"},
           "regex_findall": {"params": ("abc123", "b.+"),
                             "validate": ["bc123"]},
           "regex_replace": {"params": ("abc123", "b", "x"),
                             "validate": "axc123"},
           "regex_search": {"params": ("abc123", "b"),
                            "validate": "b"},
           "relpath": {"params": "/abc123",
                       "validate": lambda x: "abc123" in x},
           "sha1": {"params": "abc123",
                    "validate": "6367c48dd193d56ea7b0baad25b19455e529f5ee"},
           "shuffle": {
               "validate": lambda _filter: hasattr(_filter, "__call__")},
           "splitext": {"params": "abc123.py",
                        "validate": ("abc123", ".py")},
           "strftime": ({"params": "%X",
                         "validate": lambda x: ":" in x},
                        {"params": ("%X", 1572978300),
                         "validate": lambda x: ":" in x}),
           "subelements": {"params": ([{'x': ['thing1', 'thing2']}], "x"),
                           "validate": lambda x: 'thing1' in x[0]},
           "ternary": {"params": (False, 'x', 'y'),
                       "validate": 'y'},
           "to_datetime": {"params": "2019-01-01 00:00:00",
                           "validate": lambda x: hasattr(x, 'now')},

           "to_json": {"params": {"abc": 123},
                       "validate": '{"abc": 123}'},
           "to_nice_json": {"params": {"abc": 123},
                            "validate": '{\n    "abc": 123\n}'},
           "to_nice_yaml": {"params": {"abc": 123},
                            "validate": "abc: 123\n"},
           "to_uuid": {"params": "abc123",
                       "validate": "cc57d48d-0f04-5d7b-9535-dd01cd362a70"},
           "to_yaml": {"params": {"abc": 123}, "validate": "{abc: 123}\n"},
           "type_debug": {"params": {"abc": 123}, "validate": "dict"},
           "win_basename": {"params": "/abc/123.py", "validate": "123.py"},
           "win_dirname": {"params": "c:/something/progra~1/abc.py",
                           "validate": "c:/something/progra~1"},
           "win_splitdrive": {"params": "c:/progra~1",
                              "validate": ('c:', '/progra~1')}}


def test_extension_loads(jinja2_env):
    assert set(FILTERS.keys()).issubset(set(jinja2_env.filters.keys())), (
        "Missing filters from defined list"
    )


@pytest.mark.parametrize("key,tests", FILTERS.items())
def test_filter_return_types(jinja2_env, key, tests):
    class DNE(object):
        pass

    if isinstance(tests, tuple):
        for test in tests:
            test_filter_return_types(jinja2_env, key, test)

    if "params" in tests:
        if isinstance(tests["params"], tuple):
            if "kwargs" in tests:
                rslt = jinja2_env.filters[key](
                    *tests["params"], **tests["kwargs"]
                )
            else:
                rslt = jinja2_env.filters[key](*tests["params"])
        else:
            if "kwargs" in tests:
                rslt = jinja2_env.filters[key](
                    tests["params"], **tests["kwargs"]
                )
            else:
                rslt = jinja2_env.filters[key](tests["params"])
    else:
        rslt = DNE

    if "validate" in tests:
        if rslt is not DNE:
            if hasattr(tests["validate"], '__call__'):
                assert tests["validate"](rslt)
            else:
                assert tests["validate"] == rslt
        elif hasattr(tests["validate"], '__call__'):
            assert tests["validate"](rslt)
        else:
            assert False, "No valid check for {}".format(key)


def test_extension_collision(recwarn):
    env = Environment()
    env.filters["b64decode"] = lambda x: "test string"

    env.add_extension("jinja2_ansible_filters.AnsibleCoreFiltersExtension")
    warn = recwarn.pop()
    assert isinstance(warn.message, RuntimeWarning), (
        "Warning not thrown for collision"
    )
